<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Controlador Paciente
//Route::get('controlador','PacienteController@index');
Route::get('/Paciente','PacienteController@index'); 
Route::post('/GuardarPaciente','PacienteController@guardar');
//Route::post('/GuardarPaciente',function(){
        //echo 'Hola como estas';});*//


// Controlador Historico
Route::get('/Historico','HistoricoController@index'); 
Route::post('/GuardarHistorico','HistoricoController@guardar');
Route::post('/ExaminarHistorico','HistoricoController@examinar');


// Controlador Item
Route::get('/Item','ItemController@index'); 
Route::post('/GuardarItem','ItemController@guardar');
