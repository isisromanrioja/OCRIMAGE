<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('ci',15);
            $table->string('nombre',50);
            $table->string('appaterno',50);
            $table->string('apmaterno',50);
            $table->timestamp('fechanac');
            $table->string('direccion',80);
            $table->string('celular',10);
            $table->string('fijo',10);
            $table->string('email',80);
             
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente');
    }
}
