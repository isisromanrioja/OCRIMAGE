<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('historico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idpaciente')->unsigned();
            $table->string('ruta', 100);
            $table->string('titulo', 50);
          //  $table->timestamp('fecha');
            $table->foreign('idpaciente')->references('id')->on('paciente');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('historico');
    }

}
