<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idhistorico');
            $table->string('nombre', 5);
            $table->string('tipo', 50);
            $table->longText('ocr');
             $table->foreign('idhistorico')->references('id')->on('historico');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('item');
    }

}
