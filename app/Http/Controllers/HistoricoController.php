<?php

namespace App\Http\Controllers;

use App\model\Historico;
use Illuminate\Http\Request;

class HistoricoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $Historico = new Historico();
        $Lhistorico = $Historico->Listar();
        return view('Historico.index', compact('LHistorico'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function show(Historico $historico) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function edit(Historico $historico) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Historico $historico) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\Historico  $historico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Historico $historico) {
        //
    }

    public function guardar(Request $request) {

        /*$data = $request->get('Historico');
        $historico = new Historico(request()->all());
        $historico->save();
        $LHistorico = $historico->Listar();
        return view('Historico.index', compact('LHistorico'));*/
        
        
        //dd(request()->all());
       // $filename = $photo->store('photos');
        $foto = $request->ruta;
        $filename =$foto->store ('fotos');
        echo $filename;
    }

    public function examinar(Request $request) {
            
       
    }
}
