<?php

namespace App\Http\Controllers;

use App\Model\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //return view('Paciente.index');
        $Paciente = new Paciente();
        $LPacientes = $Paciente->Listar();
        return view('Paciente.index', compact('LPacientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function show(Paciente $paciente) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paciente $paciente) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente) {
        //
    }

    public function guardar(Request $request) {

        //dd(request()->all());
        $data = $request->get('Paciente');
        // dd($paciente);
        $paciente = new Paciente(request()->all());
        $paciente->save();
        $LPacientes = $paciente->Listar();
        return view('Paciente.index', compact('LPacientes'));
        // return view('Paciente.index'); 
        // dd($paciente);
    }

}
