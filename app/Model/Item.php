<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

    protected $table = 'item';
    protected $primaryKey = 'id';
    protected $fillable = ['idhistorico', 'nombre', 'tipo', 'ocr'];

    public function Listar() {
        return Item::orderBy('idhistorico', 'asc')->get();
    }

}
