<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Historico extends Model {

    protected $table = 'historico';
    protected $primaryKey = 'id';
    protected $fillable = ['idpaciente', 'ruta', 'titulo'];

    public function Listar() {
        return Historico::orderBy('idpaciente', 'asc')->get();
    }

}
