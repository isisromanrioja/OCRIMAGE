<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table='paciente';
    protected $primaryKey='id';
    protected $fillable = ['ci','nombre','appaterno','apmaterno','fechanac','direccion','celular','fijo','email'];   
    public function Listar(){
    return Paciente::orderBy('id','asc')->get();                        
    }
            
    
}
