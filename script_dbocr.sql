-- Database: dbocr

-- DROP DATABASE dbocr;
/*CREATE DATABASE dbocr
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    LC_CTYPE = 'Spanish_Spain.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    
COMMENT ON DATABASE dbocr
    IS 'Sistema de archivos JPG de historico clinico';
 */  
  -- creando tablas de la Base de Datos
  -- create sequence idpaciente start 1;
  CREATE TABLE paciente(
      id           serial not null,
      -- id int not null default nextval('idpaciente'),
      ci           varchar(15) not null,
      nombre       varchar(50) not null,
      appaterno    varchar(50) not null,
      apmaterno    varchar(50) not null,
      fechanac     date not null,
      direccion    varchar(80),
      celular      varchar(10),
      fijo         varchar(10),
      email        varchar(80),
      
	  CONSTRAINT  paciente_id_key UNIQUE(id),
      PRIMARY KEY (id)
  );
  CREATE TABLE historico(
      id           serial not null,
      idpaciente   int not null,
      ruta         varchar(100) not null,
      titulo       varchar(50),
      -- fecha        date not null, -- timestamp
      
	  CONSTRAINT   historico_id_key UNIQUE(id),
	  PRIMARY KEY (id),
	  FOREIGN KEY (idpaciente) REFERENCES paciente(id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  CREATE TABLE item(
      id           serial not null,
      idhistorico  int not null,
      nombre       varchar(50) not null,-- imagen01
      tipo         varchar(10) not null,-- jpg
      ocr          text,
	  
	  CONSTRAINT  item_id_key UNIQUE(id),
	  PRIMARY KEY (id),
	  FOREIGN KEY (idhistorico) REFERENCES historico(id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  
-- INSERT INTO paciente VALUES(default,'456','Tito', 'Gonzales', 'Pedraza','27/03/1980','Calle Totai','77023555',null,null);
select * from paciente;
-- select * from historico;
-- select * from item;